
import React,{useContext,useEffect} from 'react';
import UserContext from '../userContext';
import Banner from '../components/Banner';


export default function Logout(){

	const {setUser,unsetUser} = useContext(UserContext);
	console.log(unsetUser);
	console.log(setUser);

	unsetUser();

	//add empty dependenct array to run useEffect only on initial render.	
	useEffect(()=>{
			//set the global user state to its initial values
			//updating a state uncluded in the context with its setter function.
			setUser({
				id:null,
				isAdmin:null
			})

	},[])

	let bannerContent = {

		title: "See you later!",
		description: "You have logged out of SLEEP.PLS",
		buttonCallToAction: "Go Back to Home Page",
		destination: "/"

	}; 	

	return(
		<Banner bannerProp={bannerContent}/>	
	)
}

