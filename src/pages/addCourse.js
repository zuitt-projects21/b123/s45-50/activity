import React,{useState,useEffect,useContext} from 'react';
import {Form, Button} from 'react-bootstrap';
import UnauthorizedAccess from './UnauthorizedAccess';

import Swal from 'sweetalert2'

import UserContext from '../userContext';

import {Redirect,useHistory} from 'react-router-dom';



export default function AddCourse() {

	const{user} = useContext(UserContext);
	console.log(user)

	const history = useHistory();

	const [name,setName] = useState("");
	const [description,setDescription] = useState("");
	const [price,setPrice] = useState("");

	const [isActive,setIsActive] = useState(false);

	useEffect(()=>{
		if(name !== "" && description !== "" && price !== ""){
			setIsActive(true)
		} else{
			setIsActive(false)
		}

	},[name,description,price])

	let userToken = localStorage.getItem("token");
	console.log(userToken);		


	function createCourse(e){
		e.preventDefault();

		fetch('http://localhost:4000/courses/',{

			method: 'POST',
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${userToken}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price 
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data.name){
				Swal.fire({
					icon: "success",
					title: `Course: ${data.name} Successfully Added!`,
					text: `You will be redirected back to the courses page.`
				})

				history.push('/courses')
			} else {
				Swal.fire({
					icon: "error",
					title: "Failed to add course.",
					text: data.message
				})
			}	
		})

		setName("");
		setDescription("");
		setPrice(0);

	}

	return (
		user.isAdmin
		? 
			<>
				<h1 className="my-5 text-center">Add Course</h1>
				<Form onSubmit={e => createCourse(e)}>
					<Form.Group>
						<Form.Label>Course Name:</Form.Label>
						<Form.Control type="text" value={name} onChange={e => {setName(e.target.value)}} placeholder="Enter Course Name" required/>
					</Form.Group>
					<Form.Group>
						<Form.Label>Description:</Form.Label>
						<Form.Control type="text" value={description} onChange={e => {setDescription(e.target.value)}} placeholder="Enter Description" required/>
					</Form.Group>
					<Form.Group>
						<Form.Label>Price:</Form.Label>
						<Form.Control type="number" value={price} onChange={e => {setPrice(e.target.value)}}  placeholder="Enter Course Price" required/>
					</Form.Group>
					{
					isActive 
					? <Button variant="primary" type="submit">Add Course</Button>
					: <Button variant="secondary" disable>Add Course</Button>
					}
				</Form>
			</>	
		: <UnauthorizedAccess/> 	


	)

}