import React,{useState,useEffect,useContext} from 'react';
import {Card,Button,Row,Col} from 'react-bootstrap';
import {useParams,useHistory,Link} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../userContext';

export default function ViewCourse(){

	//useParams will allow us to retrieve any parameter in our URL. returns an object
	//useParams() will return an object containing the URL params (named after how you indicated it in App.js)
	const {courseId} = useParams();
	//console.log(courseId);

	//get the global user state from our context
	const {user} = useContext(UserContext);
	//save useHistory and its methods as history var to use its methods.
	const history = useHistory();

	//save course details in a state
	const [courseDetails,setCourseDetails] = useState({

		name: null,
		description: null,
		price: null

	});

	//add use effect to get course details
	useEffect(()=>{

		fetch(`http://localhost:4000/courses/getSingleCourse/${courseId}`)
		.then(res=>res.json())
		.then(data=>{
			if(data.message){
				Swal.fire({
					icon: "error",
					title: "Course Unavailable",
					text: data.message
				})
			} else{
				setCourseDetails({
					name: data.name,
					description: data.description,
					price: data.price
				})
			}

		})

	},[courseId])

	console.log(courseDetails)

	const enroll = (courseId) => {

		console.log(courseId);

		fetch(`http://localhost:4000/users/enroll`,{
				method: 'POST',
				headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
					courseId: courseId
				})
			})
		.then(res=>res.json())
		.then(data=>{
			console.log(data);
		})


	}

	return(

			<Row className="mt-5">
				<Col>
					<Card>	
						<Card.Body className="text-center">
							<Card.Title>{courseDetails.name}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{courseDetails.description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>{courseDetails.price}</Card.Text>
							<Card.Subtitle>Schedule:</Card.Subtitle>
							<Card.Text>8am - 5pm</Card.Text>
							{
								user.isAdmin === false
								?
								<Button variant="primary" block onClick={() => enroll(courseId)}>Enroll</Button>
								:
								<Link className="btn btn-danger btn-block" to="/login">Login to Enroll</Link>
							}

						</Card.Body>	
					</Card>	
				</Col>	
			</Row>	

		)

}