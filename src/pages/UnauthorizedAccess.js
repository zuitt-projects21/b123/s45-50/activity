import React from 'react';
import Banner from '../components/Banner';

export default function UnauthorizedAccess() {
	let errorProp = {

		title: "Unauthorized Access",
		description: "The page you are trying to access is unavailable",
		buttonCallToAction: "View our Courses",
		destination: "/courses"

	};

	return(
		<>
			<Banner bannerProp={errorProp} />
		</>
	)

}
