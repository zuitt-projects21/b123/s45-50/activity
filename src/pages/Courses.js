import React,{useState,useEffect} from 'react';
/*import coursesData from '../data/coursesData';*/
import Course from '../components/Course';

export default function Courses() {

	/*console.log(coursesData);*/

	/*Each instance of a component is independent from one another.
	
	So for example, we called multiple course components, each of those instances are independent from each other. Therefore allowing us to have re-usable UI components. (is enabled through the use of props)	
	
	*/

	/*let sampleProp1 = "I am sample 1";
	let sampleProp2 = "I am sample 2";*/

	//create a state with empty array as init value:
	const [coursesArray,setCoursesArray] = useState([]);
	useEffect(()=>{

		fetch("http://localhost:4000/courses/getActiveCourses")
		.then(res => res.json())
		.then(data => {

			//resulting new array from mapping the data(which is an array of course documents) will be set into our coursesArray state with its setter function.
			setCoursesArray(data.map(course => {
		
				return (
					<Course key={course._id} courseProp={course}/>
				)
					
			}))
		})
	},[])	

	console.log(coursesArray);

	return (
			<>
				<h1 className="my-5">Available Courses</h1>
				{coursesArray}
			</>
		)

}