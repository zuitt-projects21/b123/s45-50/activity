/*parent of all components. index.js is an entry point*/

import React,{useState,useEffect} from 'react';
import AppNavBar from './components/AppNavBar';

//import react router dom components fro simulated page routing:
import {BrowserRouter as Router} from 'react-router-dom';
import {Route, Switch} from 'react-router-dom';

//import user provider - the provider component  
import {UserProvider} from './userContext';

//import pages
import Home from './pages/Home';
import Courses from './pages/Courses';
import ViewCourse from './pages/ViewCourse';
import Register from './pages/Register';
import Login from './pages/Login';
import NotFound from './pages/NotFound';
import Logout from './pages/Logout';
import AddCourse from './pages/addCourse';




//import container from react-bootstrap
import {Container} from 'react-bootstrap';

//import app.css in this component
import './App.css'

export default function App() {

	const [user,setUser] = useState({

		id: null,
		isAdmin: null

	})

	//useEffect to fetch our user's details:
	useEffect(()=>{

		fetch('http://localhost:4000/users/getUserDetails',{

			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}

		})
		.then(res => res.json())
		.then(data => {

			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})

		})

	},[])

	console.log(user)

	/*
		ReactJS is a single page application(SPA),however, we can actually simulate the changing of pages. We dont actualoly create new pages, what we do is just switch components/pages according to their assigned routes. ReactJS and react-router-diom package just mimics or mirrors how HTML accesses its URLs.

		With this, we dont have to reload the page each time we switch pages.

		react-router-dom has 3 main components to simulate the changing of pages:
			Router 
				wrapping our Router component around our other components will allow routing within our app.
			Switch
				Allows to switch/change our page components
			Route
				assigns a path which will trigger the change/switch of components to render.	
	*/

	//function to clear localStorage on logout
	const unsetUser = () => {	
		localStorage.clear();
	}

	return (
			<>
				<UserProvider value={{user,setUser,unsetUser}}>	
					<Router>				
					<AppNavBar />
						<Container>
							<Switch>
								<Route exact path="/" component={Home}/>
								<Route exact path="/courses" component={Courses}/>
								<Route exact path="/courses/:courseId" component={ViewCourse}/>
								<Route exact path="/login" component={Login}/>
								<Route exact path="/register" component={Register}/>
								<Route exact path="/logout" component={Logout}/>	
								<Route exact path="/addCourse" component={AddCourse}/>	
								<Route component={NotFound}/>
							</Switch>
						</Container>
					</Router>
				</UserProvider>
			</>	
		)

}