let coursesData = [

	{
		id: "wdc001",
		name: "PHP-Laravel",
		description:"Lorem ipsum commodo commodo nisi occaecat est occaecat esse ea ut quis elit officia reprehenderit ex sit anim adipisicing.",
		price: 45000,
		onOffer: true		
	},
	{
		id: "wdc002",
		name: "Python-Django",
		description:"Lorem ipsum veniam dolore dolor exercitation ut in magna aliqua duis sunt irure ut irure ut ut et in deserunt anim consectetur incididunt enim ea elit pariatur et magna cillum incididunt ullamco ut.",
		price: 50000,
		onOffer: true		
	},
	{
		id: "wdc003",
		name: "Java-Springboot",
		description:"Lorem ipsum voluptate nulla sunt laboris non fugiat qui adipisicing veniam non ullamco ut amet pariatur dolor ea aliquip ut duis proident adipisicing irure esse ut elit laborum in in laborum et laboris dolor officia nisi labore.",
		price: 55000,
		onOffer: true		
	},
	{
		id: "wdc004",
		name: "Nodejs-MERN",
		description:"Tempor dolore tempor consectetur consectetur ut anim dolore do dolore magna mollit ut cillum minim in ut veniam id dolor eu eu non laboris magna qui sit sunt.",
		price: 60000,
		onOffer: false		
	}
]

export default coursesData;