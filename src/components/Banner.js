import React from 'react';
import {Row,Col,Button,Jumbotron} from 'react-bootstrap';
	//{} deconstructing the react-bootstrap modules.
//Link component creates an anchor tag, however, it does not use href and instead it will just commit to switching our page instead of reloading
import {Link} from 'react-router-dom';


export default function Banner({bannerProp}){

	console.log(bannerProp)

	return (

		<Row>
			<Col>
				<Jumbotron>
				<h1>{bannerProp.title}</h1>
				<p>{bannerProp.description}</p>
				<Link to={bannerProp.destination} className="btn btn-primary">{bannerProp.buttonCallToAction}</Link>
				</Jumbotron>
			</Col>
		</Row>

	)

}

//NOTE: put href to home just for activity 48.

/*notes: Row and Col are components from React Boostrap module. They create div elements with bootstrap classes.

react-bootstrap components create react elements with bootstrap classes
*/