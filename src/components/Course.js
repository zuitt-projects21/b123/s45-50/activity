import React,{useState,useEffect} from 'react';
import {Button,Card} from 'react-bootstrap';
import {Link} from 'react-router-dom';


export default function Course({courseProp}) {

	console.log(courseProp);
	//States in Reactjs are ways to store information within a component. The advantage of a state from a variable is that variables do not retain updated information when the component is updated. We destructure the array returned by useState() and assign them in variables. setterFunction can be user defined, but by convention, it should be named after what state it is updating.
	/*const [count,setCount] = useState(0);
	const [seats,setSeats] = useState(30);
	const [isActive,setIsActive] = useState(true);*/

	//useState is a react hook which allows us to create a state and its setter function. useState actually returns 2 items in an array, with the argument in it becoming the initial value of our state variable.

/*	console.log(useState(0));*/

	//This console log repeats whenever the enroll button is pressed because whenever a state is updated, the component re-renders.
		//Rendering in Reactjs is the act of showing our elements in the component. It is the run of our component. Whenever our component rerenders, it actually runs the component function again
	//variables are best used for static components(that doesnt need to change over time.)

	//Conditional Rendering is when we are able to show/hide elements based on a condition. 	
	/*console.log(courseProp);*/

	/*
	Every time the seats state is updated, we will check if the seats state is equal to 0. and if it is, we wll change the value of the isActive state to false. (solves problem for multiple users on a page)
	*/
	
	/*useEffect(()=>{

		if(seats === 0){
			setIsActive(false);
		}

	},[seats]);

	console.log(isActive);*/

	/*let varCount = 0;
	console.log(count);
	console.log(seats);*/


/*	function enroll(){
		alert("Hello!")
		setCount(count+1);
		setSeats(seats-1);
				
		varCount++;
		console.log(varCount);
		
	}*/

	return(
		
		<Card>
			<Card.Body>
				<Card.Title>{courseProp.name}</Card.Title>
				<Card.Subtitle>Description:</Card.Subtitle>
				<Card.Text>{courseProp.description}</Card.Text>
				<Card.Subtitle>Price:</Card.Subtitle>
				<Card.Text>PHP {courseProp.price}</Card.Text>
				<Link className="btn btn-primary" to={`/courses/${courseProp._id}`}>View Course</Link>
			</Card.Body>
		</Card>

	)

}

//note: only ternary conditions can be embedded