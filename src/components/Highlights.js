import React from 'react';
import {Row,Col,Card} from 'react-bootstrap';

export default function Highlights() {
//note: xs and md are breakpoints
	return (

			<Row>
				<Col xs={12} md={4}>
					<Card className="cardHighlights">
						<Card.Body>
							<Card.Title>
								<h2>Learn from Home</h2>
							</Card.Title>
							<Card.Text>
								Qui non consectetur reprehenderit ex qui dolore pariatur sint et incididunt culpa elit do amet culpa amet dolore in qui in in consectetur duis eu in irure voluptate adipisicing nisi officia.
							</Card.Text>
						</Card.Body>
					</Card>
				</Col>

				<Col xs={12} md={4}>
					<Card className="cardHighlights">
						<Card.Body>
							<Card.Title>
								<h2>Study Now, Pay Later</h2>
							</Card.Title>
							<Card.Text>
								Quis consectetur ex ut anim in do est sit ullamco magna velit sed duis ullamco amet ex proident nisi minim reprehenderit sint sint.
							</Card.Text>
						</Card.Body>
					</Card>
				</Col>

				<Col xs={12} md={4}>
					<Card className="cardHighlights">
						<Card.Body>
							<Card.Title>
								<h2>Be Part of Our Community</h2>
							</Card.Title>
							<Card.Text>
								Dolor proident reprehenderit qui in culpa do consequat sed ullamco consequat adipisicing consectetur in dolor ut ad sit cupidatat irure.
							</Card.Text>
						</Card.Body>
					</Card>
				</Col>
			</Row>

		)

}