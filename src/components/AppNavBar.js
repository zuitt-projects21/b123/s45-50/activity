import React,{useContext} from 'react';
import {Navbar,Nav} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import UserContext from '../userContext';

export default function AppNavBar() {

	//useContext hook will allow us to "unwrap" useContext returns an object after unwrapping our context.	
	/*console.log(useContext(UserContext))	*/
	//Destructure the returned object by useContext and get our global user state.
	const {user} = useContext(UserContext);
	console.log(user);
	/*
	if your component has children(like text content), it should have a closing tag. If not, you can self close.

	classes in reactjs are added as className instead of class

	"as" prop allows components to be treated as if they are another component. A components with the as prop will be able to gain access to another components properties and functionalities
	*/

	return (

		<Navbar bg="primary" expand="lg">
			<Navbar.Brand as={Link} to="/">Zuitt</Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-nav"/>
			<Navbar.Collapse id="basic-navbar-nav">
				<Nav className="ml-auto">
					<Nav.Link as={Link} to="/">Home</Nav.Link>
					<Nav.Link as={Link} to="/courses">Courses</Nav.Link>
					{
						user.id
						? 
							user.isAdmin
							? 
								<>
								<Nav.Link as={Link} to="/addCourse">Add Course</Nav.Link>
								<Nav.Link as={Link} to="/logout">Logout</Nav.Link>
								</>	
							: <Nav.Link as={Link} to="/logout">Logout</Nav.Link>
						: 
						<>
							<Nav.Link as={Link} to="/register">Register</Nav.Link>
							<Nav.Link as={Link} to="/login">Login</Nav.Link>
						</>
					}
					
				</Nav>
			</Navbar.Collapse>
		</Navbar>

		)

}